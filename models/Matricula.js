module.exports = class Matricula {

  constructor(nome, idade) {
    this._nome = nome
    this._idade = idade
  }

  get nome() {
    return this._nome
  }

  get idade() {
    return this._idade
  }

  set nome(nome) {
    this._nome = nome
  }

  set idade(idade) {
    this._idade = idade
  }

}