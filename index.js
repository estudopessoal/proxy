const Matricula    = require('./models/Matricula')
const ProxyFactory = require('./services/ProxyFactory')

const matricula = ProxyFactory.create(new Matricula('Allan', 27), ['nome', 'idade'], () => console.log('!'))

console.log(matricula.nome)
console.log(matricula.idade)