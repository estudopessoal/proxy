module.exports = class ProxyFactory {

  static create(model, props=[], action) {
    return new Proxy(model, {
      get(target, prop, receives) {

        if (props.includes(prop) && typeof(target[prop]) === typeof(Function)) {
          return function () {

            Reflect.apply(target[prop], target, arguments)
            return action(target)

          }
        }

        return Reflect.get(target, prop, receives)

      },

      set(target, prop, value, receives) {

        if (props.includes(prop))
          return action(target)

        return Reflect.set(target, prop, value, receives)

      }
    })
  }

}